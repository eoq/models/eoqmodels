# EOQ Meta-model Repository

This is the collection of meta-models used within the implementations of EOQ. Each meta-model is maintained in an individual repository. 

## Content
* workspacemodel: The meta-model of the EOQ Workspace MDB
* xmlresourcemodel: The meta-model of XML Resources as used in the Workspace MDB

# EOQ

[Essential Object Query (EOQ)](https://gitlab.com/eoq/essentialobjectquery) is a language to interact remotely and efficiently with object-oriented models, i.e. domain-specific models. It explicitly supports the search for patterns, as used in model transformation languages. Its motivation is an easy to parse and deterministically behaving query structure, but as high as possible efficiency and flexibility. EOQ’s capabilities and semantics are similar to the Object-Constraint-Language (OCL), but it supports in addition transactional model modification, change events, and error handling.  

Main Repository: https://gitlab.com/eoq/essentialobjectquery

EOQ user manual: https://gitlab.com/eoq/doc 

